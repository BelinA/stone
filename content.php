<?
if (!session_id()) session_start();
if (!$auth->isAuth())  {
	return;
}
?>
<div class="row">
	<div class="col-12">
		<div class="alert alert-primary text-center m-1" role="alert">
			Вы зашли как <?=$_SESSION["user_right"] ? 'менеджер' : 'администратор'?> (Имя пользователя: <?=$auth->getLogin()?>)
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal" data-id="-1" data-edtable="password" data-placement="left" title="Сменить пароль пользователя"><i class="fa fa-edit" aria-hidden="true"></i></button>
		</div>
	</div>
</div>
<?
if ($_SESSION["user_right"] == 0) {
	// Вошли как администратор
	//
	// Добавим новый тип ресурса в БД
	if (isset($_GET['res_name']) && isset($_GET['res_type']) && isset($_GET['res_right'])){
		if (strlen($_GET['res_name']) > 2) {
			$id = NULL;
			$query = "INSERT INTO resources (id, type, right_resource, name_resource, id_block) VALUES (?,?,?,?,?)";
			if (!($statement = $db->prepare($query))) exit('Error query');
			$statement->bind_param("iiisi", $id, $_GET['res_type'], $_GET['res_right'], $_GET['res_name'], $_GET['res_block']);
			if ($statement->execute()) {
				echo "<div class='row'><div class='col-12 alert alert-success alert-dismissible fade show' role='alert'><span>Новый ресурс с именем '{$_GET['res_name']}' успешно создан</span><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div></div>";
			}
			$statement->close();
		}
	}

	// Добавим новый тип блока отображения в БД
	if (isset($_GET['block_name'])) {
		$id = NULL;
		$query = "INSERT INTO blocks (id, name) VALUES (?,?)";
		if (!($statement = $db->prepare($query))) exit('Error query');
		$statement->bind_param("is", $id, $_GET['block_name']);
		if ($statement->execute()) {
			echo "<div class='row'><div class='col-12 alert alert-success alert-dismissible fade show' role='alert'><span>Новый блок отображения успешно создан</span><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div></div>";
			unset($_GET['block_name']);
		}
		$statement->close();
	}
?>
<div class="row">
	<div class="col-sm">
		<form>
			<div class="form-row">
			  <div class="col-12">
				<input required="required" type="text" class="form-control" placeholder="Введите название для нового ресурса" name="res_name">
			  </div>
			  <div class="col-12">
				  <small><strong>Выберите тип ресурса</strong></small>
				  <select class="custom-select" name="res_type">
					  <option value="0" selected>Выпадающий список</option>
					  <option value="1">Текстовое поле</option>
				  </select>
			  </div>
			  <div class="col-12">
				  <small><strong>Выберите для кого доступен ресурс для редактирования</strong></small>
				  <select class="custom-select" name="res_right">
					  <option value="0" selected>Только аминистратор</option>
					  <option value="1">Все пользователи</option>
				  </select>
			  </div>
			  <div class="col-12">
				  <small><strong>Выберите блок отображения</strong></small>
				  <select class="custom-select" name="res_block">
					  	<?
							// Запишем информацию о блоках для отображения в таблице ресурсов
							$id_blocks_ar = [];

							// Прочитаем все типы блоков
							$query = "SELECT * from blocks ORDER BY name DESC";
							if (!($statement_b = $db->prepare($query))) exit('Error query');
							$statement_b->execute();
							$statement_b->bind_result($id, $name);
							$i = 0;
							while ($statement_b->fetch()) {
							$id_blocks_ar[$id] = $name;
						?>
							<option value="<?=$id?>" <?=($i == 0) ? 'selected' : ''?>><?=$name?></option>
						<?
							$i++;
							}
							$statement_b->close();
						?>
				  </select>
			  </div>
			</div>
			<div class="row m-3">
				<div class="col-12 text-center">
					<button type="submit" class="btn btn-primary"><i class="fa fa-file-o" aria-hidden="true"></i> Создать</button>
				</div>
			</div>
		</form>
	</div>

	<div class="col-sm">
		<form>
			<div class="form-row">
			  <div class="col-12">
				<input required="required" type="text" class="form-control" placeholder="Введите название для нового блока отображения" name="block_name">
				<small>* Используется для группировки ресурсов</small>
			  </div>
			</div>
			<div class="row m-3">
				<div class="col-12 text-center">
					<button type="submit" class="btn btn-primary"><i class="fa fa-file-o" aria-hidden="true"></i> Создать</button>
				</div>
			</div>
		</form>
	</div>

</div>

<div class="row">
	<div class="col">
		<div class="table-responsive">
			<table class="table table-hover">
			  <thead class="thead-dark">
				<tr>
				  <th scope="col">Название Ресурсa</th>
				  <th scope="col">Тип</th>
				  <th scope="col">Доступ</th>
				  <th scope="col" style="width: 200px;">Действие</th>
				</tr>
			  </thead>
			  <tbody>
	<?
		// Прочитаем все типы для отображения в таблице
		$query = "SELECT * from resources";
		if (!($statement = $db->prepare($query))) exit('Error query');
		$statement->execute();
		$statement->bind_result($id, $type, $right_resource, $name_resource, $id_block);

		// Заполним используемые типы блоков в ресурсах, нужно для возможности удаления ресурсов
		$use_id_blocks = [];

		while ($statement->fetch()) {
			if (!in_array($id_block, $use_id_blocks)) {
				$use_id_blocks[] = $id_block;
			}
	?>
				<tr>
					<td><?=$name_resource?><small> (Блок отображения: <?=$id_blocks_ar[$id_block]?>)</small></td>
				  <td><?=(intval($type)) ? "Текстовое поле" : "Выпадающий список"?></td>
				  <td><?=(intval($right_resource)) ? "Все" : "Администратор"?></td>
				<td>
					<button type="button" class="btn btn-success" data-toggle="modal" data-target="#editModal" data-id="<?=$id?>" data-edtable="resources"><i class="fa fa-edit" aria-hidden="true"></i></button>
					<?
					if (intval($type) === 0) {
					?>
					<button type="button" class="btn btn-success" data-toggle="modal" data-target="#editModal" data-id="<?=$id?>" data-edtable="resources_list"><i class="fa fa-bars" aria-hidden="true"></i></button>
					<?
					}
					?>
					<button type="button" class="btn btn-danger" onclick="rs_delete('<?=$id?>')"><i class="fa fa-times" aria-hidden="true"></i></button>
				</td>
				</tr>
	<?
		}
		$statement->close();
	?>
			  </tbody>
			</table>
		</div>
	</div>
</div>


<div class="row">
	<div class="col">
		<div class="table-responsive">
			<table class="table table-hover">
			  <thead class="thead-dark">
				<tr>
				  <th scope="col" style="width: 200px;">#</th>
				  <th scope="col">Блок отображения</th>
				  <th scope="col" style="width: 200px;">Действие</th>
				</tr>
			  </thead>
			  <tbody>
	<?
			foreach($id_blocks_ar as $key => $value) {
	?>
				<tr>
					<td><?=$key?></td>
					<td><?=$value?></td>
				<td>
					<button type="button" class="btn btn-success" data-toggle="modal" data-target="#editModal" data-id="<?=$key?>" data-edtable="blocks"><i class="fa fa-edit" aria-hidden="true"></i></button>
					<?
						if (!in_array($key, $use_id_blocks)) {
					?>
						<button type="button" class="btn btn-danger" onclick="rs_delete('<?=$key?>', 'blocks')"><i class="fa fa-times" aria-hidden="true"></i></button>
					<?
						} else {
					?>
						<button type="button" class="btn btn-danger" disabled="true" data-placement="left" title="Блок используется и не может быть удален"><i class="fa fa-times" aria-hidden="true"></i></button>
					<?
						}
					?>
				</td>
				</tr>
	<?
		}
	?>
			  </tbody>
			</table>
		</div>
	</div>
</div>

<?
} elseif ($_SESSION["user_right"] == 1) {
	// Вошли как менеджер
	// Прочитаем все документы

	$id_sort = 'ASC';
	$id_sort_char = 'down';
	if (isset($_GET['sort'])) {
		if ($_GET['sort'] == "ASC") {
			$id_sort = 'DESC';
			$id_sort_char = 'up';
		} else {
			$id_sort = 'ASC';
			$id_sort_char = 'down';
		}
	}

	if (isset($_GET['search_string'])) {
		$search_string = "WHERE fields LIKE '%{$_GET['search_string']}%' ";
	} else {
		$search_string = '';
	}

	$query = "SELECT * FROM records {$search_string} ORDER BY id {$id_sort}";

	if (!($statement = $db->prepare($query))) exit('Error query');
	$statement->execute();
	$statement->bind_result($id, $fields, $user, $record_date);
?>
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-sm m-1 text-left">
					<form class="form-inline">
						<input class="form-control" type="search" placeholder="Введите значение текстового поля" size="40" name="search_string" value="<?=isset($_GET['search_string']) ? $_GET['search_string'] : ''?>">
					</form>
				</div>
				<div class="col-sm m-1 text-right">
						<button type="button" class="btn btn-success" data-toggle="modal" data-target="#editModal" data-id="-2" data-edtable="records"><i class="fa fa-file-o" aria-hidden="true"></i> Создать новый документ</button>
				</div>
			</div>

			<div class="table-responsive">
				<table class="table table-hover">
				  <thead class="thead-dark">
					<tr>
						<th scope="col" style="width: 200px;">#  <a href="?sort=<?=$id_sort?><?=isset($_GET['search_string']) ? '&search_string='.$_GET['search_string'] : ''?>"><i class="fa fa-caret-<?=$id_sort_char?>" aria-hidden="true"></i></th>
					  <th scope="col">Дата создания</th>
					  <th scope="col">Кем создан</th>
					  <th scope="col" style="width: 200px;">Действие</th>
					</tr>
				  </thead>
				  <tbody>
		<?
			while ($statement->fetch()) {
		?>
					<tr>
						<td><?=$id?></td>
						<td><?=$record_date?></td>
						<td><?=$user?></td>
					<td>
						<button type="button" class="btn btn-success" data-toggle="modal" data-target="#editModal" data-id="<?=$id?>" data-edtable="records"><i class="fa fa-edit" aria-hidden="true"></i></button>
						<button type="button" class="btn btn-success" onclick="rs_print('<?=$id?>')"><i class="fa fa-print" aria-hidden="true"></i></button>
						<button type="button" class="btn btn-danger" onclick="rs_delete('<?=$id?>', 'records')"><i class="fa fa-times" aria-hidden="true"></i></button>
					</td>
					</tr>
		<?
			}
		?>
				  </tbody>
				</table>
			</div>
		</div>
	</div>
<?
}
	$db->close();
?>

<script>
$(document).ready(function(){

	$('#editModal').on('shown.bs.modal',
	function (event) {
	  let button = $(event.relatedTarget); // Кнопка, что спровоцировало модальное окно
	  let id = button.data('id'); // Извлечение информации из данных-* атрибутов
	  let tbl = button.data('edtable'); // Извлечение информации из данных-* атрибутов
	  let modal = $(this);
	  	$.post('/ajax/edit.php', {id: id, tbl: tbl},
			function(result){
				modal.find('#idbody').html(result);
				if (id > 0) {
					modal.find('#exampleModalLabel2').html('Редактирование записи');
				} else if ( id == -1) {
					modal.find('#exampleModalLabel2').html('Введите новый пароль');
				} else if ( id == -2) {
					modal.find('#exampleModalLabel2').html('Создать новый документ');
				}
		});
	});

});

/* Печать документа*/
	function rs_print(id) {
		window.open('print.php?id='+id, '_blank');
	}
	/* Удаляем */
	function rs_delete(id, name = 'resources') {
		if (!confirm("Вы действительно хотите удалить запись?")) { return; }
		$.post('/ajax/delete.php', {id: id, name: name},
		function(result){
				if (result == 777) {
					location.href ='/';
				} else {
					alert('Не удалось удалить запись №'+id+' !');
				}
		});
	}

	/* Сохранение при изменении записи */
	function save_change(){
	let fr = $('#editAllFields').serializeArray();
		$.post('/ajax/save.php', {fr: fr},
		function(result){
				if (result == 777) {
					location.href ='/';
				} else {
					if ( result == 6006) {
						alert('Пароль не изменен. Придумайте пароль посложнее!');
					} else {
						alert('Не удалось сохранить запись!');
					}
				}
		});
	}

	/* Удалить весь список ресурса */
	function clear_options(id){
		$.post('/ajax/clear.php', {id: id});
		location.href ='/';
	}

</script>

<!-- Модалка редактирования/добавления записей -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel2"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="idbody">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Отмена</button>
        <button type="button" class="btn btn-primary" onclick="save_change()"><i class="fa fa-save" aria-hidden="true"></i> Сохранить изменения</button>
      </div>
    </div>
  </div>
</div>