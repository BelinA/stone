<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
if (!session_id()) session_start();

include_once ($_SERVER['DOCUMENT_ROOT'].'/config.php');
$db = new mysqli($DB_SERVER, $DB_USER_NAME, $DB_USER_PASS, $DB_NAME);
mysqli_set_charset($db, "utf8");
if ($db->connect_error) {
	exit('Error DB connect');
}

?>

<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Курсовая работа "Создание сайта туристической фирмы"</title>

	<link href="css/bootstrap.css" rel="stylesheet" type='text/css'>
        <link href="css/style.css" rel="stylesheet" type='text/css'>
        <link rel="stylesheet" href="/font-awesome/css/font-awesome.min.css">
		<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="js/popper.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
</head>

<body>
    <?php
        include_once 'classes/header.php';

        $auth = new AuthClass();

        if (isset($_POST["login"]) && isset($_POST["password"])) {
            if (!$auth->auth($_POST["login"], $_POST["password"])) {
                echo "<p class=\"text-danger\">Логин и пароль введен не правильно!</p>";
            }
        }

        if (isset($_GET["is_exit"])) {
            if ($_GET["is_exit"] == 1) {
                $auth->out();
                echo '<script>window.location.href = "/";</script>';
            }
        }
    ?>
        <div class="container-fluid">
		<div class="row-fluid">


		<?php if ($auth->isAuth()) {
		    //echo "<div class=\"row\"> ". $auth->getLogin()."&nbsp";
		    //echo "<a href=\"?is_exit=1\">Выйти</a></div>";
		}
		else {
		?>
			<div class="col"><div style="position: fixed; up: 0px; left: 0px; opacity: 0.2; z-index: -1; max-width: 100%; height: auto; transform: rotate(38deg);"><img src="image/main.gif" class="img-fluid"></div></div>
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col">
                                <form method="post" action="" >
                                 <fieldset>
                                     <legend><p class="text-info text-center">Курсовая работа "Создание сайта рабочего места менеджера туристической фирмы"</p></legend>
                                  <div class="form-group">
                                   <label for="inputLogin">Логин</label>
                                   <input required="required" id="inputLogin" placeholder="Введите логин" class="form-control" type="text" name="login" value="<?php echo (isset($_POST["login"])) ? $_POST["login"] : null; ?>" autofocus>
                                  </div>
                                  <div class="form-group">
                                   <label for="inputPassword">Пароль</label>
                                   <input required="required" type="password" id="inputPassword" placeholder="Введите пароль" class="form-control" name="password" value="">
                                  </div>
                                  <button type="submit" class="btn btn-primary btn-block">Войти</button>
                                 </fieldset>
                                </form>
                            </div>
                            <div class="col-2"></div>
                        </div>
		<?php } ?>

		<?php include_once 'content.php';?>
		<?php include_once 'footer.php';?>


		</div>
	</div>
</body>
</html>
