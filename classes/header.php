<?php
if (!session_id()) session_start();

class AuthClass {

	public function isAuth() {
        return isset($_SESSION["is_auth"]) ? $_SESSION["is_auth"] : false;
    }

    public function auth($login, $password) {
		global $DB_SERVER, $DB_USER_NAME, $DB_USER_PASS, $DB_NAME;
		$db = new mysqli($DB_SERVER, $DB_USER_NAME, $DB_USER_PASS, $DB_NAME);
		mysqli_set_charset($db, "utf8");
		$query = "SELECT name, password, user_right FROM users WHERE name='".trim($login)."' AND password='". trim($password)."' LIMIT 1";
		if (!($statement = $db->prepare($query))) exit('Error query');
		$statement->execute();
		$statement->bind_result($user_name, $user_password, $user_right);
		$statement->fetch();
		$statement->close();
		$db->close();

        if ($login == $user_name && $password == $user_password) {
            $_SESSION["is_auth"] = true;
            $_SESSION["login"] = $user_name;
			$_SESSION["user_right"] = $user_right;
            return true;
        }
        else {
            $_SESSION["is_auth"] = false;
            return false;
        }
    }

    public function getLogin() {
        if ($this->isAuth()) {
            return $_SESSION["login"];
        }
    }

    public function out() {
        $_SESSION = [];
        session_destroy();
    }
}