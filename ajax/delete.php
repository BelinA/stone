<?php

if (!isset($_POST['id']) || !isset($_POST['name'])){
	exit('06661');
}

$id = $_POST['id'];

include_once $_SERVER['DOCUMENT_ROOT'].'/config.php';

$db = new mysqli($DB_SERVER, $DB_USER_NAME, $DB_USER_PASS, $DB_NAME);

if (!$db->connect_error) {

	$query = "DELETE FROM {$_POST['name']} WHERE id=?";
	if (!($statement = $db->prepare($query))) exit('0779');
	if (!$statement->bind_param("i", $id)) exit('07791');
	$result = $statement->execute() ? 777 : '0666';
	$statement->close();
	$db->close();
} else {
	exit('Error DB connect');
}

echo $result;

