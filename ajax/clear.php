<?php
if (!isset($_POST['id'])) {
	exit ('Нет данных!');
}

$id_resource = $_POST['id'];

include_once $_SERVER['DOCUMENT_ROOT'].'/config.php';

$db = new mysqli($DB_SERVER, $DB_USER_NAME, $DB_USER_PASS, $DB_NAME);

if (!$db->connect_error) {
	$query = "DELETE FROM options WHERE id_resource=?";
	if (!($statement = $db->prepare($query))) exit('0779');
	if (!$statement->bind_param("i", $id_resource)) exit('07791');
	$result = $statement->execute() ? 777 : '0666';
	$statement->close();
	$db->close();
} else {
	exit('Error DB connect');
}

echo $result;

