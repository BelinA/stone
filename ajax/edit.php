<?php

if (!isset($_POST['id']) || !isset($_POST['tbl'])){
	exit('06661');
}

$idd = $_POST['id'];
$result = '<form id="editAllFields">';
$result .= '<input name="id_for_save" hidden="true" value="'.$idd.'">';

include_once $_SERVER['DOCUMENT_ROOT'].'/config.php';

$db = new mysqli($DB_SERVER, $DB_USER_NAME, $DB_USER_PASS, $DB_NAME);
mysqli_set_charset($db, "utf8");

if ($db->connect_error) {
	exit('Error DB connect');
}

if ( $_POST['tbl'] == 'resources' ){
	$query = "SELECT * from resources WHERE id={$idd}";
	if (!($statement = $db->prepare($query))) exit('Error query');
	$statement->execute();
	$statement->bind_result($id, $type, $right_resource, $name_resource, $id_block);
	$statement->fetch();
	$statement->close();

	$result .='<input required="required" type="text" class="form-control" placeholder="Введите название для нового ресурса" name="res_name" value="'.$name_resource.'">';
	$result .='<small><strong>Выберите тип ресурса</strong></small>
				<select class="custom-select" name="res_type">';

		if ($type == 0) {
			$result .='<option value="0" selected>Выпадающий список</option>';
		} else {
			$result .='<option value="0">Выпадающий список</option>';
		}

		if ($type == 1) {
			$result .='<option value="1" selected>Текстовое поле</option>';
		} else {
			$result .='<option value="1">Текстовое поле</option>';
		}

	$result .= '</select>';
	$result .='<small><strong>Выберите для кого доступен ресурс для редактирования</strong></small>
				  <select class="custom-select" name="res_right">';
		if ($right_resource == 0) {
			$result .='<option value="0" selected>Только аминистратор</option>';
		} else {
			$result .='<option value="0">Только аминистратор</option>';
		}

		if ($right_resource == 1) {
			$result .='<option value="1" selected>Все пользователи</option>';
		} else {
			$result .='<option value="1">Все пользователи</option>';
		}
	$result .= '</select>';
	$result .='<small><strong>Выберите блок отображения</strong></small>
				  <select class="custom-select" name="res_block">';

		// Прочитаем все типы блоков
		$query = "SELECT * from blocks ORDER BY name DESC";
		if (!($statement_b = $db->prepare($query))) exit('Error query');
		$statement_b->execute();
		$statement_b->bind_result($id, $name);
		while ($statement_b->fetch()) {
		$id_blocks_ar[$id] = $name;
		$str = ($id == $id_block) ? "selected" : " ";
	$result .='<option value="'.$id.'" '.$str.'>'.$name.'</option>';

		}
		$statement_b->close();

	$result .='</select>';
} elseif ( $_POST['tbl'] == 'blocks' ) {
		$query = "SELECT name from blocks WHERE id={$idd}";
		if (!($statement_b = $db->prepare($query))) exit('Error query');
		$statement_b->execute();
		$statement_b->bind_result($name);
		$statement_b->fetch();
		$statement_b->close();
	$result .= '<input required="required" type="text" class="form-control" placeholder="Введите название для нового блока отображения" name="block_name" value="'.$name.'">';
} elseif ($_POST['tbl'] == "resources_list") {
		$query = "SELECT * from options WHERE id_resource={$idd} ORDER BY name ASC";
		if (!($statement_c = $db->prepare($query))) exit('Error query');
		$statement_c->execute();
		$statement_c->bind_result($id, $id_resource, $name);
		$result .= '<input required="required" type="text" class="form-control" placeholder="Новый элемент списка" name="option_new">';
		while ($statement_c->fetch()) {
			$result .= '<input required="required" type="text" class="form-control" placeholder="Введите название элемента" name="'.$id.'" value="'.$name.'">';
		}
		$result .= '<button type="button" class="btn btn-danger m-1" onclick="clear_options('.$id_resource.')">Очистить список</button>';
		$statement_c->close();
} elseif ($_POST['tbl'] == "password") {
	$result .= '<input required="required" type="password" class="form-control" placeholder="Введите новый пароль" name="password">';
} elseif ($_POST['tbl'] == "records") {
	$result .= '<div class="contener">';
	$result .= '<div class="row">';
		if ($idd > -2) {
			// Редактирование документа
			$query = "SELECT fields from records WHERE id={$idd}";
			if (!($statement_h = $db->prepare($query))) exit('Error query');
			$statement_h->execute();
			$statement_h->bind_result($fields);
			$statement_h->fetch();
			$statement_h->close();
			$fr_js = json_decode($fields, TRUE);
		}

	// Создадим функцию для заполнение значения поля если это редактирование
	function set_value($id){
		global $fr_js;
		foreach ($fr_js as $key) {
			if ($key['name'] == $id) {
				return $key['value'];
			}
		}
		return '';
	}

	// Создадим функцию для заполнения списка если это редактирование
	function set_option($id){
		global $fr_js;
		foreach ($fr_js as $key) {
			if ($key['value'] == $id) {
				return 'selected';
			}
		}
		return '';
	}
	// Запишем информацию о блоках для отображения в таблице ресурсов
			$id_blocks_ar = [];

			// Прочитаем все типы блоков
			$query = "SELECT * from blocks";
			if (!($statement_b = $db->prepare($query))) exit('Error query');
			$statement_b->execute();
			$statement_b->bind_result($id, $name);
			while ($statement_b->fetch()) {
				$id_blocks_ar[$id] = $name;
			}
			$statement_b->close();

			// Запишем информацию о выпадающих меню
			$id_options_ar = [];

			$query = "SELECT * FROM options";
			if (!($statement_b = $db->prepare($query))) exit('Error query');
			$statement_b->execute();
			$statement_b->bind_result($id, $id_resource, $name);
			while ($statement_b->fetch()) {
				$id_options_ar[$id]['id'] = $id;
				$id_options_ar[$id]['id_resource'] = $id_resource;
				$id_options_ar[$id]['name'] = $name;
			}
			$statement_b->close;

			$query = "SELECT * from resources ORDER BY id_block ASC";
			if (!($statement = $db->prepare($query))) exit('Error query');
			$statement->execute();
			$statement->bind_result($id, $type, $right_resource, $name_resource, $id_block);
			$bl = -1;
			$i = 0;
			while ($statement->fetch()) {
				if ( $bl !== $id_block) {
					if ($i > 0) {
						$result .= '<br></div>';
					}
					$result .= '<div class="col-sm-3 border border-primary rounded m-1">';
					$result .= '<div>'.$id_blocks_ar[$id_block].'</div>';
					$bl = $id_block;
				}

				// Отрисуем поля
					if ($type == 0) {
						// Тип ресурса, это выпадающий список (0)
						$result .='<div class="col-12 m-1">';
						$result .='<small><strong>'.$name_resource.'</strong></small>';
						$result .='<select class="custom-select" name="'.$id.'">';
							foreach ($id_options_ar as $key) {
								if ($id == $key['id_resource']) {
									$result .='<option value="'.$key['id'].'" '.set_option($key['id']).'>'.$key['name'].'</option>';
								}
							}
						$result .='</select>';
						$result .='<br></div>';

					} else {
						// 	Тип ресурса, это текстовое поле (1)
						$result .='<div class="col-12 m-1">';
						$result .='<small><strong>'.$name_resource.'</strong></small>';
						$result .= '<input required="required" type="text" class="form-control" name="'.$id.'" value="'.set_value($id).'">';
						$result .='<br></div>';
					}

				$i++;
			}
			$result .= '</div>';
			$statement->close();

	$result .= '</div>';
	$result .= '</div>';
}

$db->close();
$result .= '</form>';
echo $result;