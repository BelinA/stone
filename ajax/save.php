<?php
if (!session_id()) session_start();

if (!isset($_POST['fr'])) {
	exit ('Нет данных!');
}

$fr = $_POST['fr'];

include_once $_SERVER['DOCUMENT_ROOT'].'/config.php';

$db = new mysqli($DB_SERVER, $DB_USER_NAME, $DB_USER_PASS, $DB_NAME);
mysqli_set_charset($db, "utf8");

if ($db->connect_error) {
	exit('Error DB connect');
}

if ($fr[1]['name'] == "block_name") {
	// Это Блок отображения
	$query = "UPDATE blocks SET name='{$fr[1]['value']}' WHERE id={$fr[0]['value']}";
} elseif ($fr[1]['name'] == "res_name") {
	// Это ресурс
	$query = "UPDATE resources SET type='{$fr[2]['value']}', right_resource='{$fr[3]['value']}', name_resource='{$fr[1]['value']}', id_block='{$fr[4]['value']}' WHERE id={$fr[0]['value']}";
} elseif ($fr[1]['name'] == "option_new") {
	// Это список ресурса
	if (strlen($fr[1]['value']) > 0) {
		$id_add = NULL;
		$query = "INSERT INTO options (id, id_resource, name) VALUES (?,?,?)";
		if (!($statement_e = $db->prepare($query))) exit('Error query');
		$statement_e->bind_param("iis", $id_add, $fr[0]['value'], $fr[1]['value']);
		$statement_e->execute();
		$statement_e->close();
	}
	unset($fr[0]);
	unset($fr[1]);

	foreach ($fr as $val){
		$query = "UPDATE options SET name='{$val['value']}' WHERE id={$val['name']}";
		if (!($statement_f = $db->prepare($query))) exit('Error query');
		$statement_f->execute();
		$statement_f->close();
	}
	exit('777');
} elseif ($fr[1]['name'] == "password") {
	// Это смена пароля пользователя
	if (strlen($fr[1]['value']) > 2){
		$query = "UPDATE users SET password='{$fr[1]['value']}' WHERE name='{$_SESSION["login"]}'";
	} else {
		exit('6006');
	}
} elseif($fr[0]['value'] == -2){
	// Это добавление нового документа
	unset($fr[0]);
	$fr_js = json_encode($fr, JSON_UNESCAPED_UNICODE);
	$id_add = NULL;
	$query = "INSERT INTO records (id, fields, user) VALUES (?,?,?)";
	if (!($statement_e = $db->prepare($query))) exit('Error query');
	$statement_e->bind_param("iss", $id_add, $fr_js, $_SESSION["login"]);
	$statement_e->execute();
	$statement_e->close();
	exit('777');
} else {
	// Это редактирование документа
	$idd = $fr[0]['value'];
	unset($fr[0]);
	$fr_js = json_encode($fr, JSON_UNESCAPED_UNICODE);
	$query = "UPDATE records SET fields='{$fr_js}' WHERE id={$idd}";
	if (!($statement_f = $db->prepare($query))) exit('Error query');
	$statement_f->execute();
	$statement_f->close();
	exit('777');
}
if (!($statement_b = $db->prepare($query))) exit('Error query');
$result = $statement_b->execute() ? 777 : 6662;
$statement_b->close();

$db->close();
echo $result;